FROM roboxes/rhel8
LABEL maintainer="itnmunoz@gmail.com"
RUN subscription-manager register --username itnmunoz --password m1u2n3oz \
 && subscription-manager attach --auto
RUN yum install java-11-openjdk-devel.x86_64
RUN useradd -d /opt/tomcat -s /bin/nologin tomcat \
 && wget https://ftp.cixug.es/apache/tomcat/tomcat-9/v9.0.43/bin/apache-tomcat-9.0.43.tar.gz \
 && tar -zxvf apache-tomcat-*.tar.gz \
 && mv apache-tomcat-*/* /opt/tomcat/ \
 && chown -R tomcat:tomcat /opt/tomcat/
ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.10.0.9-4.el8_3.x86_64 \
    CATALINA_HOME=/opt/tomcat
ENV PATH $CATALINA_HOME/bin:$PATH
#COPY conf $CATALINA_BASE
#COPY webapps $CATALINA_BASE/webapps
EXPOSE 8080
CMD ["catalina.sh", "run"]