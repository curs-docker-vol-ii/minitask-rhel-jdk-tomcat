# Minitask-Rhel-Jdk-Tomcat

Dockerfile RHEL  + OpenJDK + Tomcat 9

# 1
subscription-manager register

subscription-manager attach --auto

#yum search jdk
yum install java-11-openjdk-devel.x86_64

#alternatives --list | grep ^java
#JAVA_HOME /usr/lib/jvm/java-11-openjdk-11.0.10.0.9-4.el8_3.x86_64/bin/

useradd -d /opt/tomcat -s /bin/nologin tomcat

#wget https://mirrors.estointernet.in/apache/tomcat/tomcat-9/v9.0.34/bin/apache-tomcat-9.0.34.tar.gz

wget https://ftp.cixug.es/apache/tomcat/tomcat-9/v9.0.43/bin/apache-tomcat-9.0.43.tar.gz

tar -zxvf apache-tomcat-*.tar.gz
mv apache-tomcat-*/* /opt/tomcat/

chown -R tomcat:tomcat /opt/tomcat/

#COPY src/tomcat.service /etc/systemd/system/

systemctl daemon-reload

systemctl enable tomcat

systemctl start tomcat